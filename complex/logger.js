import winston from 'winston';

// https://github.com/winstonjs/winston#logging
// { emerg: 0, alert: 1, crit: 2, error: 3, warning: 4, notice: 5, info: 6, debug: 7}
const level = process.env.LOG_LEVEL || 'debug';

function formatParams(info) {
  const { timestamp, level, message, ...args } = info;
  const ts = timestamp.slice(0, 19).replace('T', ' ');

  return `${ts} ${level}: ${message} ${
    Object.keys(args).length ? JSON.stringify(args, '', '') : ''
  }`;
}

// https://github.com/winstonjs/winston/issues/1135
const developmentFormat = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(formatParams)
);

const productionFormat = winston.format.combine(
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(formatParams)
);

let logger;

if (process.env.NODE_ENV !== 'production') {
  logger = winston.createLogger({
    level: level,
    format: developmentFormat,
    transports: [new winston.transports.Console()],
  });
} else {
  logger = winston.createLogger({
    level: level,
    format: productionFormat,
    transports: [
      new winston.transports.File({
        filename: 'logs/error.log',
        level: 'error',
      }),
      new winston.transports.File({ filename: 'logs/combined.log' }),
    ],
  });
}

export { logger };
export default logger;
