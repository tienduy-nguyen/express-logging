import express from 'express';
import { logger } from './logger.js';

const PORT = 3000;
const app = express();

function loggerMiddleware(req, res, next) {
  if (res.statusCode < 400) {
    logger.http(`${req.method} ${req.originalUrl}`);
  }
  next();
}

app.use(loggerMiddleware);

app.get('/', function (req, res) {
  res.send('Hi there!');
});

app.get('/error', function (req, res) {
  throw new Error('Problem Here!');
});

// All errors are sent back as JSON
app.use((err, req, res, next) => {
  // Fallback to default node handler
  if (res.headersSent) {
    next(err);
    return;
  }

  logger.error(err.message, { url: req.originalUrl });

  res.status(500);
  res.json({ error: err.message });
});

// Start server
app.listen(PORT, function () {
  logger.info('Example app listening at http://localhost:3000');
});
