import winston from 'winston';
import 'winston-daily-rotate-file';
import clc from 'cli-color';
import safeStringify from 'fast-safe-stringify';

// https://github.com/winstonjs/winston#logging
// {  error: 0, warn: 1, info: 2, http: 3, verbose: 4, debug: 5, silly: 6}
const level = process.env.NODE_ENV !== 'production' ? 'debug' : 'info';

const nestLikeColorScheme = {
  info: clc.greenBright,
  error: clc.red,
  warn: clc.yellow,
  debug: clc.magentaBright,
  verbose: clc.cyanBright,
};

function nestLikeParams(info) {
  const { context, level, timestamp, message, ...meta } = info;
  const color = nestLikeColorScheme[level] || ((text) => text);
  return (
    `${color(`[NEST]`)} ` +
    `${clc.yellow(level.charAt(0).toUpperCase() + level.slice(1))}\t` +
    ('undefined' !== typeof timestamp
      ? `${new Date(timestamp).toLocaleString()} `
      : '') +
    ('undefined' !== typeof context
      ? `${clc.yellow('[' + context + ']')} `
      : '') +
    `${color(message)} - ` +
    `${safeStringify(meta)}`
  );
}

function nestLikeProductionParams(info) {
  const { context, level, timestamp, message, ...meta } = info;
  return (
    `${level.charAt(0).toUpperCase() + level.slice(1)}\t` +
    ('undefined' !== typeof timestamp
      ? `${new Date(timestamp).toLocaleString()} `
      : '') +
    ('undefined' !== typeof context ? `${'[' + context + ']'} ` : '') +
    `${message} - ` +
    `${safeStringify(meta)}`
  );
}

function formatParams(info) {
  const { timestamp, level, message, ...args } = info;
  const ts = timestamp.slice(0, 19).replace('T', ' ');

  return `${ts} ${level}: ${message} ${
    Object.keys(args).length ? JSON.stringify(args, '', '') : ''
  }`;
}

// https://github.com/winstonjs/winston/issues/1135
const developmentFormat = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(formatParams)
);

const productionFormat = winston.format.combine(
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(formatParams)
);
const nestLikeConsoleFormat = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(nestLikeParams)
);
const nestLikeFileFormat = winston.format.combine(
  winston.format.timestamp(),
  winston.format.align(),
  winston.format.printf(nestLikeProductionParams)
);

let logger;

if (process.env.NODE_ENV !== 'production') {
  logger = winston.createLogger({
    level: level,
    format: nestLikeConsoleFormat,
    transports: [new winston.transports.Console()],
  });
} else {
  logger = winston.createLogger({
    level: level,
    format: nestLikeFileFormat,
    transports: [
      new winston.transports.DailyRotateFile({
        filename: 'logs/errors/error-%DATE%.log',
        level: 'error',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
      }),
      new winston.transports.DailyRotateFile({
        filename: 'logs/combined/combined-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
      }),
    ],
  });
}

export { logger };
export default logger;
