import express from 'express';
import morgan from 'morgan';
import { logger } from './logger.js';

const PORT = 3000;
const app = express();

const morganFormat = process.env.NODE_ENV !== 'production' ? 'tiny' : 'common';

app.use(
  morgan(morganFormat, {
    skip: function (req, res) {
      return res.statusCode < 400;
    },
    stream: process.stderr,
  })
);

app.use(
  morgan(morganFormat, {
    skip: function (req, res) {
      return res.statusCode >= 400;
    },
    stream: process.stdout,
  })
);

app.get('/', function (req, res) {
  logger.info(`${req.method} ${req.url?.toString()}`);
  res.send('Hi there!');
});

app.get('/error', function (req, res) {
  throw new Error('Problem Here!');
});

// All errors are sent back as JSON
app.use((err, req, res, next) => {
  // Fallback to default node handler
  if (res.headersSent) {
    next(err);
    return;
  }

  logger.error(err.message, { url: req.originalUrl });

  res.status(500);
  res.json({ error: err.message });
});

// Start server
app.listen(PORT, function () {
  logger.info('Example app listening at http://localhost:3000');
});
