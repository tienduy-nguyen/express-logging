const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const rfs = require('rotating-file-stream');

const app = express();

// // create a write stream (in append mode)
// // normal write
// const accessLogStream = fs.createWriteStream(
//   path.join(__dirname, 'access.log'),
//   {
//     flags: 'a',
//   }
// );

// 1 log file per day
var accessLogStream2 = rfs.createStream('access.log', {
  interval: '1d', // rotate daily
  path: path.join(__dirname, 'log'),
});

// setup the logger
// log only 4xx and 5xx responses to console
app.use(
  morgan('dev', {
    skip: function (req, res) {
      return res.statusCode < 400;
    },
  })
);

// log all requests to access.log
app.use(
  morgan('common', {
    stream: accessLogStream2,
  })
);

app.get('/', function (req, res) {
  res.send('hello, world!');
});

app.listen(3000, () => {
  console.log('Server is running at http://localhost:3000');
});
