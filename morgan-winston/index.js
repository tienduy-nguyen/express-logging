import express from 'express';
import morgan from 'morgan';
import { logger } from './logger-stream.js';

const app = express();

app.use(
  morgan('tiny', { stream: { write: (msg) => logger.info(msg.trip()) } })
);

app.get('/', function (req, res) {
  res.send('hello, world!');
});

logger.error('some error');
app.listen(3000, () => {
  console.log('Server is running at http://localhost:3000');
});
