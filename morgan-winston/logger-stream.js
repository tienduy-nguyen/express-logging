import winston from 'winston';

// const logger = new winston.createLogger({
//   transports: [
//     new winston.transports.File({
//       level: 'info',
//       filename: './logs/all-logs.log',
//       handleExceptions: true,
//       json: true,
//       colorize: false,
//       maxsize: 5242880, //5MB
//       maxFiles: 5,
//     }),
//     new winston.transports.Console({
//       level: 'debug',
//       handleExceptions: true,
//       json: false,
//       colorize: true,
//     }),
//   ],
//   exitOnError: false,
// });

const customFormat = winston.format.printf((args) => {
  const { timestamp, level, message } = args;
  const more = args[Symbol.for('splat')];
  const moreMsg = more
    ? more.map((msg) =>
        msg instanceof Object ? JSON.stringify(msg, null, 2) : msg.toString()
      )
    : [];
  return `${timestamp} | ${level}: ${message} ${moreMsg.join(' ')}`;
});
export const logger = winston.createLogger({
  level: 'debug',
  format: winston.format.combine(winston.format.timestamp(), customFormat),
  transports: [
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/all.log' }),
  ],
});
if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        customFormat
      ),
    })
  );
}
export class LoggerStream {
  write(message, encoding) {
    logger.info(message.trim());
  }
}
